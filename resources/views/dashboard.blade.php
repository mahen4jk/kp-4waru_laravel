@extends('home')

@section('heading')
    <h1 class="mt-4">Dashboard</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    <div class="card" style="padding-bottom: 25px;">
        <div class="card-body">
            <h5 class="card-title" style="text-align:center">SELAMAT DATANG DI</h5>
            <h5 class="card-title" style="text-align:center">DASHBOARD PERPUSTAKAAN</h5>
            <p style="text-align:center"><img src="{{ url('assets/img/Spenpatru(New).png') }}" alt="SPENPATRU" width="350" height="350"></p>
            <h5 class="card-title" style="text-align:center">SMP Negeri 4 Waru</h5>
            <p style="text-align:center">Jl. Gajah Mada, Ngingas, Kec. Waru, Kabupaten Sidoarjo, Jawa Timur 61256</p>
        </div>
    </div>
@endsection

@section('content')
    <div class="row" style="padding-top: 25px;">
        <div class="col-xl-3 col-md-6">
            <div class="card bg-primary text-white mb-4">
                <div class="card-body">Siswa</div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">Lihat Detail</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-warning text-white mb-4">
                <div class="card-body">Guru & Karyawan</div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">Lihat Detail</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-success text-white mb-4">
                <div class="card-body">Koleksi Buku</div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">Lihat Detail</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-danger text-white mb-4">
                <div class="card-body">Sirkulasi</div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">Lihat Detail</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
    </div>
@endsection
