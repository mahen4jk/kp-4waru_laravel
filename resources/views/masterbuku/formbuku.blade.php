@extends('home')
@section('heading')
    <h1 class="mt-4">Form Buku</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Home</li>
        <li class="breadcrumb-item active">Form Buku</li>
    </ol>
@endsection
@section('content')
    <form action="simpanBuku" method="POST">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="form-group row">
            <label for="kd_buku" class="col-sm-2 col-form-label">Kode Buku</label>
            <div class="col-sm-10">
                <input type="text" name="kd_buku" id="kd_buku" class="form-control" placeholder="Kode Buku" value="{{ $kode_buku }}" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label for="j_buku" class="col-sm-2 col-form-label">Judul Buku</label>
            <div class="col-sm-10">
                <input type="text" name="j_buku" id="j_buku" class="form-control" placeholder="Judul Buku">
            </div>
        </div>
        <div class="form-group row">
            <label for="pengarang" class="col-sm-2 col-form-label">Pengarang</label>
            <div class="col-sm-10">
                <input type="text" name="pengarang" id="pengarang" class="form-control" placeholder="Pengarang">
            </div>
        </div>
        <div class="form-group row">
            <label for="penerbit" class="col-sm-2 col-form-label">Penerbit</label>
            <div class="col-sm-10">
                <input type="text" name="penerbit" id="penerbit" class="form-control" placeholder="Penerbit">
            </div>
        </div>
        <div class="form-group row">
            <label for="th_terbit" class="col-sm-2 col-form-label">Tahun Terbit</label>
            <div class="col-sm-10">
                <input type="text" name="th_terbit" id="th_terbit" class="form-control" placeholder="Tahun Terbit">
            </div>
        </div>
        <div class="form-group row">
            <label for="exemplar" class="col-sm-2 col-form-label">Exemplar</label>
            <div class="col-sm-10">
                <input type="text" name="exemplar" id="exemplar" class="form-control" placeholder="Jumlah Exemplar">
            </div>
        </div>
        <div class="form-group row">
            <label for="klasifikasi" class="col-sm-2 col-form-label">Klasifikasi</label>
            <div class="col-sm-10">
                <input type="text" name="klasifikasi" id="klasifikasir" class="form-control" placeholder="Kode Klasifikasi">
            </div>
        </div>
        <div class="form-group row">
            <label for="tp_koleksi" class="col-sm-2 col-form-label">Tipe Koleksi</label>
            <div class="col-sm-10">
                <select class="form-control" id="tp_koleksi" name="tp_koleksi">
                    <option selected>=== Tipe Koleksi ===</option>
                    @foreach ($koleksi as $koleksi)
                        <option value="{{ $koleksi->id_koleksi }}">{{ $koleksi->nm_koleksi }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="jm_buku" class="col-sm-2 col-form-label">Jumlah Buku</label>
            <div class="col-sm-10">
                <input type="text" name="jm_buku" id="jm_buku" class="form-control" placeholder="Jumlah Buku">
                <small id="h_buku" class="text-muted">*Optional</small>
            </div>
        </div>
        <div class="form-group row">
            <label for="h_buku" class="col-sm-2 col-form-label">Harga Buku</label>
            <div class="col-sm-10">
                <input type="text" name="h_buku" id="h_buku" class="form-control" placeholder="Harga Buku"
                    aria-describedby="h_buku">
                <small id="h_buku" class="text-muted">*Optional</small>
            </div>
        </div>
        <div class="form-group row">
            <label for="k_buku" class="col-sm-2 col-form-label">Kondisi Buku</label>
            <div class="col-sm-10">
                <select class="form-control" id="k_buku" name="k_buku">
                    <option selected>Kondisi Buku</option>
                    <option value="Masih Bagus">Masih Bagus</option>
                    <option value="Sebagian Halaman Hilang">Sebagian Halaman Hilang</option>
                    <option value="Rusak">Rusak</option>
                </select>
                <small id="h_buku" class="text-muted">*Optional</small>
            </div>
        </div>
        <div class="form-group row">
            <label for="sinopsis" class="col-sm-2 col-form-label">Sinopsis</label>
            <div class="col-sm-10">
                <textarea id="sinopsis" class="form-control" name="sinopsis" rows="3"></textarea>
                <small id="h_buku" class="text-muted">*Optional</small>
            </div>
        </div>
        <button type="submit" name="simpan" class="btn btn-success float-right ml-2">Simpan</button>
        <button type="button" class="btn btn-danger float-right ml-2" onclick="kembali()">Kembali</button>
    </form>
    <script>
        function kembali() {
            location.href = "{{ url('dashboard/masterbuku') }}";
        }
    </script>
@endsection
