@extends('home')
@section('heading')
    <h1 class="mt-4">Data Koleksi</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Home</li>
    </ol>
    <div class="row" style="padding-bottom: 35px;">
        <div class="col-xl-5 col-md-6">
            <form method="POST" action="">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Input Kode Buku/Judul">
                    <div class="input-group-append">
                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-xl-5 col-md-6">
            <button type="button" class="btn btn-info float-right ml-8" onclick="tambahbuku()">Tambah Buku</button>
        </div>
    </div>
@endsection
@section('content')
    <table style="padding: top 25px;" class="table table-light">
        <thead class="thead-light">
            <tr>
                <th class="text-center">Kode</th>
                <th class="text-center">Judul</th>
                <th class="text-center">Pengarang</th>
                <th class="text-center">Penerbit</th>
                <th class="text-center">Klasifikasi</th>
                <th class="text-center">ID Koleksi</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($buku as $buku)
                <tr>
                    <td class="text-center">{{ $buku->kode_buku }}</td>
                    <td class="text-center">{{ $buku->j_buku }}</td>
                    <td class="text-center">{{ $buku->pengarang}}</td>
                    <td class="text-center">{{ $buku->penerbit }}</td>
                    <td class="text-center">{{ $buku->klasifikasi }}</td>
                    <td class="text-center">{{ $buku->koleksi_id }}</td>
                    <td class="text-center">
                        <a href="editBuku/{{ $buku->kode_buku }}">
                            <input type="button" class="btn btn-success" value="Edit">
                        </a>
                        <a href="hapusBuku/{{ $buku->kode_buku }}">
                            <input type="button" class="btn btn-danger" value="Hapus">
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th class="text-center">Kode</th>
                <th class="text-center">Judul</th>
                <th class="text-center">Penerbit</th>
                <th class="text-center">Pengarang</th>
                <th class="text-center">Klasifikasi</th>
                <th class="text-center">ID Koleksi</th>
                <th class="text-center">Action</th>
            </tr>
        </tfoot>
    </table>
    <script>
        function tambahbuku() {
            location.href = "formbuku";
        }

    </script>
@endsection
