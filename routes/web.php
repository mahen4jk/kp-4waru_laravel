<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\mstBukuController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

#Tampilan Awal
Route::get('dashboard',[Controller::class,'dashboard']);

#Tampil Master Buku
Route::prefix('master')->group(function () {
    Route::get('masterbuku',[mstBukuController::class,'mstBuku']);

    #Tampil Form dan Simpan Buku
    Route::get('formbuku',[mstBukuController::class,'formBuku']);
    Route::post('simpanBuku', [mstBukuController::class,'simpanBuku']);

    #Update Buku
    Route::get('editBuku/{kode_buku}',[mstBukuController::class,'kirim_edit']);
    Route::post('editBuku/updateBuku', [mstBukuController::class,'updateBuku']);

    #Delete Buku
    Route::get('hapusBuku/{kode_buku}',[mstBukuController::class,'hapusBuku']);
});

Route::prefix('transaksi')->group(function () {

});
