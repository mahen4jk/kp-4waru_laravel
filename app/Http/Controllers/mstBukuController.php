<?php

namespace App\Http\Controllers;

use App\Models\masterbuku;
use App\Models\tipekoleksi;
use Illuminate\Http\Request;
use DB;

class mstBukuController extends Controller
{
    //
    public function mstBuku()
    {
        # code...
        $buku = masterbuku::all();
        return view('masterbuku.masterbuku',['buku'=>$buku]);
    }

    public function formBuku() {
        $kd_buku = masterbuku::kode();
        $koleksi = tipekoleksi::all();
        return view('masterbuku.formbuku', compact('koleksi'),['kode_buku'=>$kd_buku]);
    }

    public function simpanBuku(Request $buku)
    {
        # code...
        $simpan = new masterbuku();
        $simpan->simpan($buku);
        return redirect('dashboard/masterbuku');
    }

    public function kirim_edit($kode)
    {
        # code...
        $koleksi = tipekoleksi::all();
        $kode_buku = DB::table('mst_buku')->where('kode_buku',$kode)->get();
        return view('masterbuku.formEditbuku',['buku'=>$kode_buku],compact('koleksi'));
    }

    public function updateBuku(Request $kode)
    {
        # code...
        $update = new masterbuku();
        $update->editBuku($kode);
        return redirect('dashboard/masterbuku');
    }

    public function hapusBuku($kode)
    {
        # code...
        $hapus = new masterbuku();
        $hapus->hapus($kode);
        return redirect('dashboard/masterbuku');
    }
}
