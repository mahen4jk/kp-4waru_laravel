<?php

namespace App\Models;

use App\Http\Controllers\mstBukuController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tipekoleksi extends Model
{
    protected $table='tp_koleksi';
    protected $primarykey=null;
    public $incrementing=false;
    public $timestamps=false;
    protected $fillable = ['id_koleksi','nm_koleksi'];
    use HasFactory;

    public function koleksi()
    {
        # code...
        return $this->hasMany(mstBukuController::class);
    }
}
