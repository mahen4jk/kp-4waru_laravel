<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class masterbuku extends Model
{
    protected $table='mst_buku';
    protected $primarykey=null;
    public $incrementing=false;
    public $timestamps=false;
    protected $fillable = ['kode_buku','j_buku','pengarang','penerbit','th_terbit','exemplar','klasifikasi','koleksi_id','jm_buku',
    'hg_buku','k_buku','sinopsis','created_at','updated_at'];
    use HasFactory;

    public static function kode()
    {
        # code...
        $kd_buku = DB::table('mst_buku')->max('kode_buku');
        $addNol = '';
        $kd_buku = str_replace("","",$kd_buku);
        $kd_buku = (int)$kd_buku + 1;
        $incrementKode = $kd_buku;

        if (strlen($kd_buku) == 1) {
            $addNol = "000";
        } elseif (strlen($kd_buku) == 2){
            $addNol = "00";
        } elseif (strlen($kd_buku == 3)) {
            $addNol = "0";
        }
        $kodebaru = "".$addNol.$incrementKode;
        return $kodebaru;
    }

    public function simpan($buku)
    {
        # code...
        DB::table('mst_buku')->insert([
            'kode_buku'=>$buku->kd_buku,
            'j_buku'=>$buku->j_buku,
            'pengarang'=>$buku->pengarang,
            'penerbit'=>$buku->penerbit,
            'th_terbit'=>$buku->th_terbit,
            'exemplar'=>$buku->exemplar,
            'klasifikasi'=>$buku->klasifikasi,
            'koleksi_id'=>$buku->tp_koleksi,
            'jm_buku'=>$buku->jm_buku,
            'hg_buku'=>$buku->h_buku,
            'k_buku'=>$buku->k_buku,
            'sinopsis'=>$buku->sinopsis,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
    }

    public function edit($kode)
    {
        # code...
        DB::table('mst_buku')->where('kode_buku',$kode)->get();
    }

    public function editBuku($buku)
    {
        # code...
        DB::table('mst_buku')->where('kode_buku',$buku->kd_buku)->update([
            'j_buku'=>$buku->j_buku,
            'pengarang'=>$buku->pengarang,
            'penerbit'=>$buku->penerbit,
            'th_terbit'=>$buku->th_terbit,
            'exemplar'=>$buku->exemplar,
            'klasifikasi'=>$buku->klasifikasi,
            'koleksi_id'=>$buku->tp_koleksi,
            'jm_buku'=>$buku->jm_buku,
            'hg_buku'=>$buku->h_buku,
            'k_buku'=>$buku->k_buku,
            'sinopsis'=>$buku->sinopsis,
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
    }

    public function hapus($kode)
    {
        # code...
        DB::table('mst_buku')->where('kode_buku',$kode)->delete();
    }
}
