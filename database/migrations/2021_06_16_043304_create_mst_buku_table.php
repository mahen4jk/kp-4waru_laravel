<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_buku', function (Blueprint $table) {
            $table->char('kode_buku',10);
            $table->char('j_buku',35);
            $table->char('pengarang',40);
            $table->char('penerbit',40);
            $table->year('th_terbit');
            $table->integer('exemplar');
            $table->char('klasifikasi',5);
            $table->char('tp_koleksi',15);
            $table->integer('hg_buku');
            $table->char('k_buku',35);
            $table->text('sinopsis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_buku');
    }
}
